# Meu Bairro Epidemio Service

#ATENÇÃO
Após o tempo do desafio foram ajustadas somentes confiugrações para o deploy, que pode ser encontrada no link:
https://meubairroepidemio-client-4wfkpf1jx-joaogms-projects.vercel.app/dashboard

## Sobre o Serviço

Este serviço é desenvolvido usando Java 17 com Spring Framework. O repositório pode ser encontrado em [meubairroepidemio-service](https://gitlab.com/joaogm/meubairroepidemio-service).

## Endpoints

### 1. Contar Atributos por Coluna Específica

- **Endpoint**: `GET /count/{attribute}/{whereColumn}`
- **Descrição**: Retorna a contagem de registros com base em um atributo específico filtrado por outra coluna.
- **Parâmetros**:
  - `attribute`: Nome da coluna que deseja contar.
  - `whereColumn`: Nome da coluna usada para filtrar a contagem.
- **Retorno**: Retorna um `Mono<Integer>` com o número de registros que correspondem ao critério.

### 2. Contar Registros por Bairro

- **Endpoint**: `GET /count/neighborhood/{whereValue}`
- **Descrição**: Retorna detalhes do bairro junto com a contagem de registros para um bairro específico.
- **Parâmetros**:
  - `whereValue`: Nome do bairro para o qual os registros devem ser contados.
- **Retorno**: Retorna um `Mono<Neighborhood>` com detalhes e contagem do bairro especificado.

### 3. Contar Registros por Bairro e Atributo Específico

- **Endpoint**: `GET /count/neighborhood/{neighborhood}/{columnValue}/{whereValue}`
- **Descrição**: Retorna detalhes e contagem de registros para um bairro específico, filtrados por uma coluna e seu valor correspondente.
- **Parâmetros**:
  - `neighborhood`: Nome do bairro.
  - `columnValue`: Coluna pela qual filtrar.
  - `whereValue`: Valor específico da coluna para filtrar os registros.
- **Retorno**: Retorna um `Mono<Neighborhood>` com detalhes e contagem de acordo com os critérios especificados.

### 4. Contar Registros por Bairro e Intervalo de Datas

- **Endpoint**: `GET /count/neighborhood/count/date/{neighborhood}/{startDate}/{endDate}`
- **Descrição**: Retorna detalhes e contagem de registros para um bairro específico dentro de um intervalo de datas.
- **Parâmetros**:
  - `neighborhood`: Nome do bairro.
  - `startDate`: Data de início do intervalo (formato ISO 8601, por exemplo, YYYY-MM-DD).
  - `endDate`: Data final do intervalo (formato ISO 8601).
- **Retorno**: Retorna um `Mono<Neighborhood>` com a contagem de registros para o intervalo de datas especificado.

### 5. Contar Registros por Intervalo de Datas

- **Endpoint**: `GET /count/neighborhood/count/date/{startDate}/{endDate}`
- **Descrição**: Retorna uma lista de bairros com a contagem de registros dentro de um intervalo de datas.
- **Parâmetros**:
  - `startDate`: Data de início do intervalo (formato ISO 8601).
  - `endDate`: Data final do intervalo (formato ISO 8601).
- **Retorno**: Retorna um `Flux<Neighborhood>` com bairros e suas contagens correspondentes para o intervalo de datas especificado.

## Estrutura de Dados do Banco

A estrutura de dados do banco foi definida de acordo com o dicionário de dados do Sinan. caso queira adicionar dados so postgresql ( bd escolinho), s epode usar um comando semelhante a este:

INSERT INTO public.dados_notificacao
(id, co_cid, dt_notificacao, co_uf_notificacao, co_municipio_residencia, nu_idade, tp_sexo, no_bairro_residencia, ano_notificacao, no_bairro_infeccao)
VALUES(nextval('dados_notificacao_id_seq'::regclass), '', '', '', '', 0, '', '', '', '');