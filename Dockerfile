# Utilizar uma imagem base que inclui o Java 17
FROM eclipse-temurin:17-jdk AS build

# Configurar o diretório de trabalho dentro do container
WORKDIR /app

# Copiar o Gradle Wrapper e os scripts necessários
COPY gradlew .
COPY gradle gradle
COPY build.gradle .
COPY settings.gradle .

# Copiar o código fonte da aplicação
COPY src src

# Conceder permissão de execução ao Gradle Wrapper
RUN chmod +x ./gradlew

# Construir a aplicação usando o Gradle Wrapper sem executar demônios
RUN ./gradlew build --no-daemon

# Utilizar uma imagem limpa para a execução
FROM eclipse-temurin:17-jre

# Configurar o diretório de trabalho para a execução da aplicação
WORKDIR /app

# Copiar o arquivo JAR gerado pela etapa de construção
COPY --from=build /app/build/libs/*.jar app.jar

# Definir o comando para executar a aplicação
CMD ["java", "-jar", "app.jar"]
