package br.com.tech.brtechmeubairroepidemio.contract;

import br.com.tech.brtechmeubairroepidemio.contract.model.Neighborhood;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/neighborhood")
@AllArgsConstructor
public class NeighborhoodController {
    private final NeighborhoodFacade neighborhoodFacade;

    @GetMapping("/count/{attribute}/{whereColumn}")
    public Mono<Integer> getCountAttribute(@PathVariable("attribute") String attribute,
                                           @PathVariable("whereColumn") String whereColumn) {
        return neighborhoodFacade.getCountAttribute(attribute, whereColumn);
    }

    @GetMapping("/count/neighborhood/{whereValue}")
    public Mono<Neighborhood> getCountByNeighborhood(@PathVariable("whereValue") String whereValue) {
        return neighborhoodFacade.getCountByNeighborhood(whereValue);
    }

    @GetMapping("/count/neighborhood/{neighborhood}/{columnValue}/{whereValue}")
    public Mono<Neighborhood> getCountByNeighborhoodAndAttribute(
            @PathVariable("whereValue") String whereValue,
            @PathVariable("columnValue") String columnValue,
            @PathVariable("neighborhood") String neighborhood) {
        return neighborhoodFacade.getCountByNeighborhoodAndAttribute(neighborhood, columnValue, whereValue);
    }

    @GetMapping("/count/neighborhood/count/date/{neighborhood}/{startDate}/{endDate}")
    public Mono<Neighborhood> getCountByNeighborhoodbyDate(
            @PathVariable("neighborhood") String neighborhood,
            @PathVariable("startDate") String startDate,
            @PathVariable("endDate") String endDate) {
        return neighborhoodFacade.getCountByNeighborhoodbyDate(neighborhood, startDate, endDate);
    }

    @GetMapping("/count/neighborhood/count/date/{startDate}/{endDate}")
    public Flux<Neighborhood> getCountByDate(
            @PathVariable("startDate") String startDate,
            @PathVariable("endDate") String endDate) {
        return neighborhoodFacade.getCountByDate(startDate, endDate);
    }
}

