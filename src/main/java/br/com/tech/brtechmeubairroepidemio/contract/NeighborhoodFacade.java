package br.com.tech.brtechmeubairroepidemio.contract;

import br.com.tech.brtechmeubairroepidemio.contract.model.Neighborhood;
import br.com.tech.brtechmeubairroepidemio.impl.NeighborhoodService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class NeighborhoodFacade {

    private final NeighborhoodService neighborhoodService;

    public Mono<Integer> getCountAttribute(String attribute, String whereColumn) {
        return neighborhoodService.getCountAttribute(attribute, whereColumn);
    }

    public Mono<Neighborhood> getCountByNeighborhood(String whereValue) {
        return neighborhoodService.getCountByNeighborhood(whereValue)
                .map(data -> Neighborhood.builder()
                        .neighborhood(whereValue)
                        .cases(data.getCases())
                        .build());
    }

    public Mono<Neighborhood> getCountByNeighborhoodAndAttribute(String neighborhood, String columnValue, String whereValue) {
        return neighborhoodService.getCountByNeighborhoodAndAttribute(neighborhood, columnValue, whereValue)
                .map(data -> Neighborhood.builder()
                        .neighborhood(neighborhood)
                        .cases(data.getCases())
                        .build());

    }

    public Mono<Neighborhood> getCountByNeighborhoodbyDate(String neighborhood, String startDate, String endDate) {
        return neighborhoodService.getCountByNeighborhoodbyDate(neighborhood, startDate, endDate)
                .map(data -> Neighborhood.builder()
                        .neighborhood(neighborhood)
                        .cases(data.getCases())
                        .build());

    }

    public Flux<Neighborhood> getCountByDate(String startDate, String endDate) {
        return neighborhoodService.getCountByDate(startDate, endDate)
                .map(data -> Neighborhood.builder()
                        .neighborhood(data.getNeighborhood())
                        .cases(data.getCases())
                        .build());
    }
}