package br.com.tech.brtechmeubairroepidemio.impl.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Neighborhood {
    private String neighborhood;
    private Integer cases;
}
