package br.com.tech.brtechmeubairroepidemio.impl;

import br.com.tech.brtechmeubairroepidemio.impl.model.Neighborhood;
import br.com.tech.brtechmeubairroepidemio.integration.NeighborhoodRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class NeighborhoodService {
    private final NeighborhoodRepository neighborhoodRepository;

    public Mono<Integer> getCountAttribute(String attribute, String whereColumn) {
        return neighborhoodRepository.countByAttribute(attribute, whereColumn);
    }

    public Mono<Neighborhood> getCountByNeighborhood(String whereColumn) {
        return neighborhoodRepository.countByNeighborhood(whereColumn)
                .map(data -> Neighborhood.builder()
                        .neighborhood(whereColumn)
                        .cases(data.getCases())
                        .build());
    }

    public Mono<Neighborhood> getCountByNeighborhoodAndAttribute(String neighborhood, String columnValue, String whereValue) {
        return neighborhoodRepository.getCountByNeighborhoodAndAttribute(neighborhood, columnValue, whereValue)
                .map(data -> Neighborhood.builder()
                        .neighborhood(neighborhood)
                        .cases(data.getCases())
                        .build());

    }

    public Mono<Neighborhood>getCountByNeighborhoodbyDate(String neighborhood, String startDate, String endDate){
        return neighborhoodRepository.getCountByNeighborhoodbyDate(neighborhood, startDate, endDate)
                .map(data -> Neighborhood.builder()
                        .neighborhood(neighborhood)
                        .cases(data.getCases())
                        .build());

    }
    public Flux<Neighborhood> getCountByDate(String startDate, String endDate) {
        return neighborhoodRepository.getCountByDate(startDate, endDate)
                .map(data -> Neighborhood.builder()
                        .neighborhood(data.getNeighborhood())
                        .cases(data.getCases())
                        .build());
    }


    }
