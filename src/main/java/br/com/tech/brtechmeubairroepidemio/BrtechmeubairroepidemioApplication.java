package br.com.tech.brtechmeubairroepidemio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrtechmeubairroepidemioApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrtechmeubairroepidemioApplication.class, args);
    }

}
