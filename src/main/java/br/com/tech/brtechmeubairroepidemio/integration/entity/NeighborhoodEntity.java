package br.com.tech.brtechmeubairroepidemio.integration.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Table("dados_notificacao")
@Data
@Builder
public class NeighborhoodEntity {

    @Id
    @Column("co_cid")
    private String coCid;

    @Column("dt_notificacao")
    private LocalDate dtNotificacao;

    @Column("co_uf_notificacao")
    private String coUfNotificacao;

    @Column("co_municipio_residencia")
    private String coMunicipioResidencia;

    @Column("nu_idade")
    private Integer nuIdade;

    @Column("tp_sexo")
    private String tpSexo;

    @Column("no_bairro_residencia")
    private String noBairroResidencia;

    @Column("ano_notificacao")
    private String anoNotificacao;

    @Column("no_bairro_infeccao")
    private String noBairroInfeccao;

}
