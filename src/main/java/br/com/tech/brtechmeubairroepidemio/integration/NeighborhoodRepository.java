package br.com.tech.brtechmeubairroepidemio.integration;

import br.com.tech.brtechmeubairroepidemio.impl.model.Neighborhood;
import lombok.AllArgsConstructor;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class NeighborhoodRepository {

    private final DatabaseClient databaseClient;


    public Mono<Integer> countByAttribute(String attribute, String whereColumn) {
        String sql = "SELECT COUNT(" + attribute + ") FROM public.dados_notificacao WHERE " + attribute + "=" + "'" + whereColumn + "'";

        return databaseClient.sql(sql)
                .map(row -> row.get(0, Integer.class))
                .first();

    }

    public Mono<Neighborhood> countByNeighborhood(String whereColumn) {
        String sql = "SELECT no_bairro_residencia, COUNT(no_bairro_residencia) FROM dados_notificacao where no_bairro_residencia  = " + "'" + whereColumn + "'" + "GROUP BY no_bairro_residencia;";

        return databaseClient.sql(sql)
                .map((row, metadata) -> new Neighborhood(row.get("no_bairro_residencia", String.class), row.get("count", Integer.class)))
                .first();

    }

    public Mono<Neighborhood> getCountByNeighborhoodAndAttribute(String neighborhood, String columnValue, String whereValue) {
        String sql = "SELECT no_bairro_residencia, " +
                "COUNT(no_bairro_residencia) " +
                "FROM dados_notificacao where no_bairro_residencia  = " + "'" + neighborhood + "'" +
                " and " + columnValue + " = " + "'" + whereValue + "'" + " GROUP BY no_bairro_residencia;";

        return databaseClient.sql(sql)
                .map((row, metadata) -> new Neighborhood(row.get("no_bairro_residencia", String.class), row.get("count", Integer.class)))
                .first();

    }

    public Mono<Neighborhood> getCountByNeighborhoodbyDate(String neighborhood, String startDate, String endDate) {
        String sql = "SELECT no_bairro_residencia, " +
                "COUNT(no_bairro_residencia) " +
                "FROM dados_notificacao where no_bairro_residencia  = " + "'" + neighborhood + "'" +
                " and dt_notificacao between " + "'" + startDate + "'" + " and " + "'" + endDate + "'" + " GROUP BY no_bairro_residencia;";

        return databaseClient.sql(sql)
                .map((row, metadata) -> new Neighborhood(row.get("no_bairro_residencia", String.class), row.get("count", Integer.class)))
                .first();
    }

    public Flux<Neighborhood> getCountByDate(String startDate, String endDate) {
        String sql = "SELECT no_bairro_residencia, " +
                "COUNT(no_bairro_residencia) " +
                "FROM dados_notificacao where dt_notificacao between " + "'" + startDate + "'" + " and " + "'" + endDate + "'" + " GROUP BY no_bairro_residencia;";

        return databaseClient.sql(sql)
                .map((row, metadata) -> new Neighborhood(row.get("no_bairro_residencia", String.class), row.get("count", Integer.class)))
                .all();


    }

}

